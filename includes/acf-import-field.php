<?php global $wpdb;
function my_acf_add_local_field_groups(){
	acf_add_local_field_group(
		array(
			'key'=>'module_post_key',
			'title'=>'Theme Customize Option',
			'fields'=>
			array(
				array(
					'key'=>'module_post_field_0',
					'label'=>'Header Option',
					'name'=>'header_option',
					'type'=>'tab',),
				array(
					'key'=>'module_post_field_1',
					'label'=>'Logo',
					'name'=>'logo',
					'type'=>'image',),
				array(
					'key'=>'module_post_field_2',
					'label'=>'Page Sidebar',
					'name'=>'page_sidebar',
					'type'=>'tab',),
				array(
					'key'=>'module_post_field_3',
					'label'=>'Sidebar Image Option',
					'name'=>'sidebar_image_option',
					'type'=>'image',),
				array(
					'key'=>'module_post_field_4',
					'label'=>'Footer',
					'name'=>'footer',
					'type'=>'tab',),
				array(
					'key'=>'module_post_field_5',
					'label'=>'Copyright Text',
					'name'=>'copyright_text',
					'type'=>'text',)
				,array(
					'key'=>'module_post_field_6',
					'label'=>'Themes Color',
					'name'=>'color_picker',
					'type'=>'tab',),
				array(
					'key'=>'module_post_field_7',
					'label'=>'Primary Color',
					'name'=>'primary_color',
					'type'=>'color_picker',
					'return_format'=>'url',),
				array(
					'key'=>'module_post_field_8',
					'label'=>'Secondary Color',
					'name'=>'secondary_color',
					'type'=>'color_picker',
					'return_format'=>'url',)
				// TASK IMAGE REMOVE
				// ,array(
				// 	'key'=>'task_image_true_flse',
				// 	'label'=>'Task Image',
				// 	'name'=>'task_image',
				// 	'type'=>'tab',),
				// array(
				// 	'key'=>'field_5f5c6b7c12f5a',
				// 	'label'=>'Task True Icon',
				// 	'name'=>'task_true_icon',
				// 	'type'=>'image',
				// 	'return_format'=>'url',),
				// array(
				// 	'key'=>'field_5f5c6ba612f5b',
				// 	'label'=>'Task False Icon',
				// 	'name'=>'task_false_icon',
				// 	'type'=>'image',
				// 	'return_format'=>'url',)
			),
			'location'=>
			array(
				array(
					array(
						'param'=>'options_page',
						'operator'=>'==',
						'value'=>'theme-general-options',),),),));

	acf_add_local_field_group(
		array(
			'key'=>'group_5f0319452973e',
			'title'=>'Module Post',
			'fields'=>
			array(0=>
				array(
					'key'=>'field_5f1a3f9f6a19f',
					'label'=>'Page Content',
					'name'=>'page_content',
					'type'=>'flexible_content',
					'instructions'=>'',
					'required'=>0,
					'conditional_logic'=>0,
					'layouts'=>
					array(
						'layout_5f1a3fa8550fc'=>
						array(
							'key'=>'layout_5f1a3fa8550fc',
							'name'=>'module_video',
							'label'=>'Module Video',
							'display'=>'block',
							'sub_fields'=>
							array(0=>
								array(
									'key'=>'field_5f1a3fcc6a1a1',
									'label'=>'Video',
									'name'=>'video',
									'type'=>'repeater',
									'instructions'=>'',
									'required'=>0,
									'conditional_logic'=>0,
									'layout'=>'table',
									'button_label'=>'Add Video',
									'sub_fields'=>
									array(0=>
										array(
											'key'=>'field_5f1a3fdd6a1a2',
											'label'=>'Video Url',
											'name'=>'video_url',
											'type'=>'text',
											'instructions'=>'',
											'required'=>0,
											'conditional_logic'=>0,),),),),),
						'layout_5f1a3ffe6a1a3'=>
						array(
							'key'=>'layout_5f1a3ffe6a1a3',
							'name'=>'module_text',
							'label'=>'Module Text',
							'display'=>'block',
							'sub_fields'=>
							array(0=>
								array(
									'key'=>'field_5f1a40086a1a4',
									'label'=>'Text',
									'name'=>'text',
									'type'=>'wysiwyg',
									'instructions'=>'',
									'required'=>0,
									'conditional_logic'=>0,
									'default_value'=>'',
									'tabs'=>'all',
									'toolbar'=>'full',
									'media_upload'=>1,
									'delay'=>0,),),),),
					'button_label'=>'Add Section',),1=>
				array(
					'key'=>'field_5f0319b503e1c',
					'label'=>'Download Worksheet',
					'name'=>'download_worksheet',
					'type'=>'repeater',
					'instructions'=>'',
					'required'=>0,
					'conditional_logic'=>0,
					'button_label'=>'',
					'sub_fields'=>
					array(0=>
						array(
							'key'=>'field_5f031a0c03e1d',
							'label'=>'Worksheet Button Title',
							'name'=>'worksheet_button_title',
							'type'=>'text',
							'instructions'=>'',
							'required'=>0,
							'conditional_logic'=>0,),1=>
						array(
							'key'=>'field_5f031a1e03e1e',
							'label'=>'Download Worksheet url',
							'name'=>'download_worksheet_url',
							'type'=>'text',
							'instructions'=>'',
							'required'=>0,
							'conditional_logic'=>0,),),),),
			'location'=>
			array(0=>
				array(0=>
					array(
						'param'=>'post_format',
						'operator'=>'==',
						'value'=>'standard',),),
				1=>
				array(0=>
					array(
						'param'=>'post_type',
						'operator'=>'!=',
						'value'=>'post',),),
				2=>
				array(0=>
					array(
						'param'=>'post_type',
						'operator'=>'!=',
						'value'=>'page',),),
				3=>
				array(0=>
					array(
						'param'=>'post_type',
						'operator'=>'!=',
						'value'=>'ontrapage',),),),
			'menu_order'=>0,
			'position'=>'normal',
			'style'=>'default',
			'label_placement'=>'top',
			'instruction_placement'=>'label',
			'hide_on_screen'=>'',
			'active'=>true,
			'description'=>'',));

	acf_add_local_field_group(array(
		'key'=>'group_5f0c4298c5cfc',
		'title'=>'Taxonomy Order',
		'fields'=> array(0=>
			array(
				'key'=>'field_5f0c43156eac9',
				'label'=>'Taxonomy Order',
				'name'=>'taxonomy_order',
				'type'=>'number',
				'instructions'=>'',
				'required'=>1,
				'conditional_logic'=>0,),),
		'location'=>
		array(0=>
			array(0=>
				array(
					'param'=>'taxonomy',
					'operator'=>'==',
					'value'=>'all',),),),
		'menu_order'=>0,
		'position'=>'normal',
		'style'=>'default',
		'label_placement'=>'top',
		'instruction_placement'=>'label',
		'hide_on_screen'=>'',
		'active'=>true,
		'description'=>'',));}
	add_action(
		'acf/init',
		'my_acf_add_local_field_groups'); 

	// MODULE TYPE
	acf_add_local_field_group(array(
		'key' => 'group_609d16922c235',
		'title' => 'Module Post',
		'fields' => array(
			array(
				'key' => 'field_609d16a81c5ad',
				'label' => 'Module Type',
				'name' => 'module_type',
				'type' => 'radio',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'video' => 'Video',
					'audio' => 'Audio',
					'survey' => 'Survey',
					'pdf' => 'PDF',
				),
				'allow_null' => 0,
				'other_choice' => 0,
				'default_value' => '',
				'layout' => 'vertical',
				'return_format' => 'value',
				'save_other_choice' => 0,
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'post',
				),
				array(
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'post',
				),
				array(
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'ontrapage',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

	acf_add_local_field_group(array(
	'key' => 'group_5fd893993bf04',
	'title' => 'Post Order No',
	'fields' => array(
		array(
			'key' => 'field_5fd894f61e260',
			'label' => 'Order',
			'name' => 'post__order',
			'type' => 'number',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_format',
				'operator' => '==',
				'value' => 'standard',
			),
		),
		array(
			array(
				'param' => 'post_type',
				'operator' => '!=',
				'value' => 'post',
			),
		),
		array(
			array(
				'param' => 'post_type',
				'operator' => '!=',
				'value' => 'page',
			),
		),
		array(
			array(
				'param' => 'post_type',
				'operator' => '!=',
				'value' => 'ontrapage',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'side',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));
	?>