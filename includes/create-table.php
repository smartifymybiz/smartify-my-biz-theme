<?php 
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
$table_name = $wpdb->prefix . 'custom_post';
$user_table_name ='user_progress';
$charset_collate = $wpdb->get_charset_collate();
if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") !== $table_name) {
    $sql = " CREATE TABLE $table_name ( id int NOT NULL AUTO_INCREMENT, post_slug varchar(50) NOT NULL, post_label varchar(50) NOT NULL, post_singular_label varchar(50) NOT NULL, 	taxonomies_slug varchar(50) NOT NULL, post_image varchar(5000) NULL DEFAULT NULL, post_sidebar VARCHAR(5000) NULL DEFAULT NULL, post_order_no int(10) NULL, PRIMARY KEY (id) ) $charset_collate; ";
	dbDelta( $sql );
}
if($wpdb->get_var("SHOW TABLES LIKE '$user_table_name'") !== $user_table_name) {
	$user = " CREATE TABLE $user_table_name ( id int NOT NULL AUTO_INCREMENT, post_id int(10) NOT NULL, user_id int(10) NOT NULL, task_check int(10) NOT NULL, post_type varchar(50) NOT NULL, PRIMARY KEY (id) ) $charset_collate; ";
	dbDelta( $user );
}



?>