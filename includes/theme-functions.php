<?php 
global $wpdb;
$table_name = $wpdb->prefix . 'custom_post';
$count_row_table = $wpdb->get_results( " SELECT post_slug FROM $table_name " );
$add_row_table = count($count_row_table) + 1;
$duplicate_slug = '';
if( 'POST' == $_SERVER['REQUEST_METHOD'] ) {

	if (isset($_POST['submit_create_posttype'])) {
		if( isset($_POST['create_post_label']) && !empty($_POST['create_post_label']) && isset($_POST['create_post_slug']) && !empty($_POST['create_post_slug']) && isset($_POST['create_post_singular_label']) && !empty($_POST['create_post_singular_label']) ){
			$post_label = $_POST['create_post_label'];
			$post_slug = $_POST['create_post_slug'];
			$singular_label = $_POST['create_post_singular_label'];
			$taxonomies_slug = $post_slug.'_module';

	        //==================== GET DATABASE OF progress AND post_checked ====================//
			$insert_querys = $wpdb->get_results( " SELECT post_slug FROM $table_name WHERE post_slug = '$post_slug' " );
	
			if (empty($insert_querys)) {
				$insert_post = "INSERT INTO $table_name (id, post_slug, post_label, post_singular_label, taxonomies_slug, post_order_no) VALUES ('', '$post_slug', '$post_label', '$singular_label', '$taxonomies_slug', '$add_row_table') ";
				$wpdb->query($insert_post);
			}else{
				echo '<script>alert("Error: The *'.$post_slug.'* slug you entered is already exist")</script>';

			}
		}
		
	}

	if (isset($_POST['btn_post_image'])) {
		
		$id = 0;
		$post_label = '';
		foreach ($_POST['post_id'] as $k => $v) {
			if($v != ''){
				$id = $v;
				$post_label = $_POST['post_label'][$k];
				$post_slug = $_POST['post_slug'][$k];
			}
		}

		if( isset($_POST['post_label']) && !empty($_POST['post_label']) && isset($id) && !empty($id)){

	       // ==================== UPDATE ADMIN LABEL ====================//
			$check_querys = $wpdb->get_results( " SELECT post_slug FROM $table_name WHERE id = '{$id}' " );
			if (($check_querys)) {
				$insert_post =  "UPDATE $table_name SET `post_label` = '{$post_label}',`post_singular_label` = '{$post_label}' WHERE `id` = {$id}";
				$wpdb->query($insert_post);
			}
		}
	}

	if(isset($_POST['btn_dummy_content'])) {
		if (isset($_POST['customRadio']) && !empty($_POST['customRadio'])) {
			$radio_value = ($_POST['customRadio']);
			require get_template_directory() . '/includes/create_dummydata.php';
		}
	}
	if(isset($_POST['delete_post_btn'])) {
		$value = '';
		if (isset($_POST['customCheck']) && !empty($_POST['customCheck'])) {
			$value = $_POST['customCheck'];
			if($value) {
				foreach($value as $check) {
					$delete_post = "DELETE FROM $table_name WHERE post_slug = '$check'";
					$wpdb->query($delete_post);
				}
			}
		}
	}
}

function extra_post_info_menu(){    
	$page_title = 'Course Settings Option';  
	$menu_title = 'Course Settings';   
	$capability = 'manage_options';   
	$menu_slug  = 'post_type_option';   
	$function   = 'sunset_theme_post_type_option';   
	$icon_url   = 'dashicons-media-code';   
    // $icon_url   = 'dashicons-clipboard';
	$position   = 81.4;    
	add_menu_page( $page_title,$menu_title,$capability,$menu_slug,$function,$icon_url,$position ); 
} 
add_action( 'admin_menu', 'extra_post_info_menu' ); 

function sunset_load_admin_scripts( $hook ){  
	if( 'toplevel_page_post_type_option' != $hook ){ return; }
	wp_register_style( 'jquery_ui_css', 'http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', false, '1.12.1', true );
	wp_register_style( 'sunset_admin_bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '4.1.3', 'all' );
	wp_register_style( 'custom_style', get_template_directory_uri() . '/css/custom-admin.css', array(), '1.2.3' , 'all' );
	wp_enqueue_style( 'jquery_ui_css' );
	wp_enqueue_style( 'sunset_admin_bootstrap' );
	wp_enqueue_style( 'custom_style' );
	wp_enqueue_style( 'custom' );

	wp_enqueue_media();

	wp_register_script( 'admin_jquery' , 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', false, '3.3.1', true );
	wp_register_script( 'admin_popper' , 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', false, '1.12.9', true );
	wp_register_script( 'jquery_ui_js' , 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', false, '1.12.1', true );
	wp_register_script( 'sunset-admin-script', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '4.1.3', true );
	wp_register_script( 'admin_custom_js',  get_template_directory_uri() . '/js/admin-script.js', array(), '1.2.3', true );

	wp_enqueue_script( 'admin_jquery' );
	wp_enqueue_script( 'admin_popper' );    
	wp_enqueue_script( 'jquery_ui_js' );    
	wp_enqueue_script( 'sunset-admin-script' );
	wp_enqueue_script( 'admin_custom_js' );
}
add_action( 'admin_enqueue_scripts', 'sunset_load_admin_scripts' );




// Define path and URL to the ACF plugin.
define( 'MY_ACF_PATH', get_stylesheet_directory() . '/plugins/acf/' );
define( 'MY_ACF_URL', get_stylesheet_directory_uri() . '/plugins/acf/' );

// Include the ACF plugin.
include_once( MY_ACF_PATH . 'acf.php' );

// Customize the url setting to fix incorrect asset URLs.
add_filter('acf/settings/url', 'my_acf_settings_url');
function my_acf_settings_url( $url ) {
	return MY_ACF_URL;
}

// (Optional) Hide the ACF admin menu item.
add_filter('acf/settings/show_admin', 'my_acf_settings_show_admin');
function my_acf_settings_show_admin( $show_admin ) {
	return true;
}
require_once get_template_directory() . '/includes/acf-import-field.php';


function sunset_theme_post_type_option(){ ?>
	<section class="post_type_section">
		<div class="row mx-0">
			<div class="col-lg-3">
				<div class="nav flex-column nav-pills" id="theme_option_tab" role="tablist" aria-orientation="vertical">
					<a class="nav-link active" id="theme_option_create_post_type_tab" data-toggle="pill" href="#create_post_type_pills" role="tab" aria-controls="create_post_type_pills" aria-selected="true">Create New Course</a>
					<a class="nav-link" id="theme_option_dummy_content_tab" data-toggle="pill" href="#dummy_content_pills" role="tab" aria-controls="dummy_content_pills" aria-selected="false">Duplicate Course</a>
					<a class="nav-link" id="theme_option_image_option_tab" data-toggle="pill" href="#image_option_pills" role="tab" aria-controls="image_option_pills" aria-selected="false">Course Options</a>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="tab-content" id="theme_optio_tabContent">
					<div class="tab-pane fade show active" id="create_post_type_pills" role="tabpanel" aria-labelledby="theme_option_create_post_type_tab">
						<div class="row mx-0">
							<div class="col-12">
								<div class="card">
									<div class="card-header d-flex align-items-center">
										<h4 class="card-header-title">Create New Course</h4>
									</div>

									<div class="card-body">
										<form method="post" action="" name="form_create_posttype" id="form_create_posttype">
											<div class="form-group row">
												<label for="create_post_slug"  class="col-sm-2 col-form-label">Slug</label>
												<div class="col-sm-5">
													<input type="text" class="form-control" name="create_post_slug" id="create_post_slug">
												</div>
											</div>
											<div class="form-group row">
												<label for="create_post_label" class="col-sm-2 col-form-label">WP Admin Label</label>
												<div class="col-sm-5">
													<input type="text" class="form-control invalid" name="create_post_label" id="create_post_label">
												</div>
											</div>
											<div class="form-group row">
												<label for="create_post_singular_label" class="col-sm-2 col-form-label">Front End</label>
												<div class="col-sm-5">
													<input type="text" class="form-control invalid" name="create_post_singular_label" id="create_post_singular_label">
												</div>
											</div>
											<button id="create-post-type" form="form_create_posttype" name="submit_create_posttype" class="button button-primary button-larg" type="submit" disabled>Submit</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="dummy_content_pills" role="tabpanel" aria-labelledby="theme_option_dummy_content_tab">
						<div class="row mx-0">
							<div class="col-12">
								<div class="card">
									<div class="card-header d-flex align-items-center">
										<h4 class="card-header-title">Install Duplicate Course</h4>
										<div class="toolbar ml-auto"></div>
									</div>
									<?php global $wpdb;
									$table_name = $wpdb->prefix . 'custom_post';
									$radio_querys = $wpdb->get_results( "SELECT * FROM $table_name ORDER BY post_order_no ASC" );
									if (!empty($radio_querys)) { ?>
										<div class="card-body">
											<small class="font-italic">Choice Post type</small>
											<form method="post" class="mt-3" action="" name="install_dummy_content" id="install_dummy_content">
												<?php foreach ($radio_querys as $radio_query) { ?>
													<div class="custom-control custom-radio">
														<input type="radio" value="<?php echo $radio_query->post_slug; ?>" id="customRadio<?php echo $radio_query->post_slug; ?>" name="customRadio" class="custom-control-input">
														<label class="custom-control-label" for="customRadio<?php echo $radio_query->post_slug; ?>"><?php echo $radio_query->post_label; ?></label>
													</div>
												<?php } ?>
												<button id="btn_dummy_content" form="install_dummy_content" name="btn_dummy_content" class="button button-primary button-larg mt-3" disabled>Install</button>
											</form>
										</div>
									<?php } ?>
									<!-- Delete Post -->
									<div class="card-header d-flex align-items-center">
										<h4 class="card-header-title">Delete Course</h4>
										<div class="toolbar ml-auto"></div>
									</div>
									<?php if (!empty($radio_querys)) { ?>
										<div class="card-body">
											<small class="font-italic">Delete Post type</small>
											<form method="post" class="mt-3" action="" name="delete_post_type" id="delete_post_type">
												<?php foreach ($radio_querys as $radio_query) { ?>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" name="customCheck[]" class="custom-control-input" id="customCheck<?= $radio_query->post_slug; ?>" value="<?= $radio_query->post_slug; ?>">
														<label class="custom-control-label" for="customCheck<?= $radio_query->post_slug; ?>"><?= $radio_query->post_label; ?></label>
													</div>
												<?php } ?>
												<button id="delete_post_btn" form="delete_post_type" name="delete_post_btn" class="button button-primary button-larg mt-3" disabled>Delete</button>
											</form>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="image_option_pills" role="tabpanel" aria-labelledby="theme_option_image_option_tab">
						<div class="row mx-0">
							<div class="col-12">
								<div class="card">
									<div class="card-header d-flex align-items-center">
										<h4 class="card-header-title">Course Options</h4>
									</div>
									<?php if (!empty($radio_querys)) { ?>
										<div class="card-body">
											<form method="post" class="mt-3" action="" name="post_image_option" id="post_image_option">
												<div class="accordion" id="tab_course_option_panal">

													<ul id="sortable_custom_post" class="list-unstyled">
														<?php foreach ($radio_querys as $radio_query) { ?>
															<li class="ui-state-default_post" data-slug="<?= $radio_query->post_slug;?>" data-order="<?= $radio_query->post_order_no;?>">	
																<div class="row no-gutters mb-2">
																	<div class="col-3">
																		<?php echo $radio_query->post_label; ?>
																	</div>
																	<div class="col-4">
																		<a class="btn-switchid button button-primary button-larg" data-collapsein="collapse_edit_<?php echo $radio_query->post_slug; ?>" data-post_id="<?php echo $radio_query->id; ?>" data-toggle="collapse" href="#collapse_edit_<?php echo $radio_query->post_slug; ?>" role="button" aria-expanded="false" aria-controls="collapse_edit_<?php echo $radio_query->post_slug; ?>">Edit Post</a>
																	</div>
																</div>
																<div class="row no-gutters mb-2">
																	<div class="col-12">
																		<div class="collapse" id="collapse_edit_<?php echo $radio_query->post_slug; ?>">
																			<div class="row no-gutters">
																				<!-- <div class="col-12 col-md-12">
																					<div class="form-group mx-0 row">
																						<label for="<?php echo $radio_query->post_slug; ?>_upload_button" class="col-sm-3 col-form-label pl-0">Slug</label>
																						<div class="col-sm-6 px-0">
																							<input type="text" name="post_slug[]" class="form-control" value="<?php echo $radio_query->post_slug; ?>">
																							
																						</div>
																					</div>
																				</div> -->
																				<div class="col-12 col-md-12">
																					<div class="form-group mx-0 row">
																						<label for="<?php echo $radio_query->post_slug; ?>_upload_button" class="col-sm-3 col-form-label pl-0">WP Admin Label</label>
																						<div class="col-sm-6 px-0">
																							<input type="text" name="post_label[]" class="form-control" value="<?php echo $radio_query->post_label; ?>">
																							<input type="hidden" name="post_id[]" value="">
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="row no-gutters">
																				<div class="col-12 col-md-12">
																					<div class="form-group mx-0 row">
																						<label for="<?php echo $radio_query->post_slug; ?>_upload_button" class="col-sm-3 col-form-label pl-0"><?php echo $radio_query->post_label; ?></label>
																						<?php if(!empty($radio_query->post_image)) { ?>
																							<div class="col-sm-6 px-0">
																								<div class="image-container active" id="<?php echo $radio_query->post_slug; ?>_picture-preview">
																									<div class="profile-picture" style="background-image: url(<?php echo $radio_query->post_image; ?>);"></div>
																								</div>
																								<input type="button" class="button button-secondary post-image-uploader d-none" data-slug="<?php echo $radio_query->post_slug; ?>" name="<?php echo $radio_query->post_slug; ?>_upload_button" value="Upload Image" id="<?php echo $radio_query->post_slug; ?>_upload_button">
																								<input type="button" id="<?php echo $radio_query->post_slug; ?>_remove_button" data-slug="<?php echo $radio_query->post_slug; ?>" name="<?php echo $radio_query->post_slug; ?>_remove_button" class="post_image_remove button button-secondary" value="Remove Image" />
																								<input type="hidden" id="<?php echo $radio_query->post_slug; ?>_image_upload_value" value="<?php echo $radio_query->post_image; ?>" name="<?php echo $radio_query->post_slug; ?>_image_upload_value">
																							</div>
																						<?php } else { ?>
																							<div class="col-sm-6  px-0">
																								<div class="image-container" id="<?php echo $radio_query->post_slug; ?>_picture-preview">
																									<div class="profile-picture" style="background-image: url();"></div>
																								</div>
																								<input type="button" class="button button-secondary post-image-uploader" data-slug="<?php echo $radio_query->post_slug; ?>" name="<?php echo $radio_query->post_slug; ?>_upload_button" value="Upload Image" id="<?php echo $radio_query->post_slug; ?>_upload_button">
																								<input type="button" id="<?php echo $radio_query->post_slug; ?>_remove_button" data-slug="<?php echo $radio_query->post_slug; ?>" name="<?php echo $radio_query->post_slug; ?>_remove_button" class="post_image_remove d-none" value="Remove Image" />
																								<input type="hidden" id="<?php echo $radio_query->post_slug; ?>_image_upload_value" name="<?php echo $radio_query->post_slug; ?>_image_upload_value">
																							</div>
																						<?php } ?>
																					</div>
																				</div>
																				<div class="col-12 col-md-12">
																					<div class="form-group row mx-0">
																						<label for="<?php echo $radio_query->post_slug; ?>_upload_button_sidebar" class="col-sm-3 col-form-label pl-0"><?php echo $radio_query->post_label; ?> Sidebar</label>
																						<?php if(!empty($radio_query->post_sidebar)) { ?>
																							<div class="col-sm-6 px-0">
																								<div class="image-container active" id="<?php echo $radio_query->post_slug; ?>_picture-preview_sidebar">
																									<div class="profile-picture" style="background-image: url(<?php echo $radio_query->post_sidebar; ?>);"></div>
																								</div>
																								<input type="button" class="pl-0 button button-secondary post-image-uploader_sidebar d-none" data-slug="<?php echo $radio_query->post_slug; ?>" name="<?php echo $radio_query->post_slug; ?>_upload_button_sidebar" value="Upload Image" id="<?php echo $radio_query->post_slug; ?>_upload_button_sidebar">
																								<input type="button" id="<?php echo $radio_query->post_slug; ?>_remove_button_sidebar" data-slug="<?php echo $radio_query->post_slug; ?>" name="<?php echo $radio_query->post_slug; ?>_remove_button_sidebar" class="post_image_remove_sidebar button button-secondary" value="Remove Image" />
																								<input type="hidden" id="<?php echo $radio_query->post_slug; ?>_image_upload_value_sidebar" value="<?php echo $radio_query->post_sidebar; ?>" name="<?php echo $radio_query->post_slug; ?>_image_upload_value_sidebar">
																							</div>
																						<?php } else { ?>
																							<div class="col-sm-6 px-0">
																								<div class="image-container" id="<?php echo $radio_query->post_slug; ?>_picture-preview_sidebar">
																									<div class="profile-picture" style="background-image: url();"></div>
																								</div>
																								<input type="button" class="button button-secondary post-image-uploader_sidebar" data-slug="<?php echo $radio_query->post_slug; ?>" name="<?php echo $radio_query->post_slug; ?>_upload_button_sidebar" value="Upload Image" id="<?php echo $radio_query->post_slug; ?>_upload_button_sidebar">
																								<input type="button" id="<?php echo $radio_query->post_slug; ?>_remove_button_sidebar" data-slug="<?php echo $radio_query->post_slug; ?>" name="<?php echo $radio_query->post_slug; ?>_remove_button_sidebar" class="post_image_remove button button-secondary d-none" value="Remove Image" />
																								<input type="hidden" id="<?php echo $radio_query->post_slug; ?>_image_upload_value_sidebar" name="<?php echo $radio_query->post_sidebar; ?>_image_upload_value_sidebar">
																							</div>
																						<?php } ?>
																					</div>
																				</div>
																			</div>
																			
																			
																		</div>
																	</div>
																</div>
															</li>
														<?php } ?>
													</ul>
												</div>
												<button id="btn_post_image" form="post_image_option" name="btn_post_image" class="button button-primary button-larg mt-3">Save</button>
											</form>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php }

?>