<?php
global $wpdb;
$post_slug = $radio_value;
$table_name = $wpdb->prefix . 'custom_post';
$select_querys = $wpdb->get_results( " SELECT * FROM $table_name WHERE post_slug = '$post_slug' " );
if (!empty($select_querys)) {
	$db_post_slug = $select_querys[0]->post_slug;
	$db_post_label = $select_querys[0]->post_label;
	$db_post_singular_label = $select_querys[0]->post_singular_label;
	$db_taxonomies_slug = $select_querys[0]->taxonomies_slug;
	function _CreateCategory() {
	    if(!term_exists('Module 1', $GLOBALS['db_taxonomies_slug'])) {
	        $term_id_1 = wp_insert_term('Module 1', $GLOBALS['db_taxonomies_slug'], array( 'description' => '', 'slug' => 'Module 1' ) );}

	    if(!term_exists('Module 2',$GLOBALS['db_taxonomies_slug'])) {
	        $term_id_2 = wp_insert_term('Module 2', $GLOBALS['db_taxonomies_slug'], array( 'description' => '', 'slug' => 'Module 2' ) );
	    }

	    /*======== add Post ==========*/
	    if(!post_exists( "Lorem ipsum dolor",'','',$GLOBALS['db_post_slug'])) {
	        $parent_term = term_exists( 'Module 1', $GLOBALS['db_taxonomies_slug'] ); // array is returned if taxonomy is given
	        $parent_term_id = $parent_term['term_id'];
	        $args = array(   // this designates the custom post ID you want to use! make sure it's unique
	            'post_title'    => 'Lorem ipsum dolor',
	            'post_excerpt' => '',
	            'post_status'  => 'publish',
	            'post_type' => $GLOBALS['db_post_slug'],
	            'post_parent' => '',
	            'post_content'      => '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',
	            'tax_input'    => array(
	                $GLOBALS['db_taxonomies_slug'] => $parent_term_id
	            ),
	        );
	        wp_insert_post($args,true);
	        add_term_meta($parent_term_id,'taxonomy_order',1);
	    }
	    if(!post_exists( "Lorem ipsum dolor 2",'','',$GLOBALS['db_post_slug'])) {
	        $parent_term = term_exists( 'Module 1', $GLOBALS['db_taxonomies_slug'] ); // array is returned if taxonomy is given
	        $parent_term_id = $parent_term['term_id'];
	        $args = array(   // this designates the custom post ID you want to use! make sure it's unique
	            'post_title'    => 'Lorem ipsum dolor 2',
	            'post_excerpt' => '',
	            'post_status'  => 'publish',
	            'post_type' => $GLOBALS['db_post_slug'],
	            'post_parent' => '',
	            'post_content'      => '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',
	            'tax_input'    => array(
	                $GLOBALS['db_taxonomies_slug'] => $parent_term_id
	            ),
	        );
	        wp_insert_post($args,true);
	    }
	    if(!post_exists( "Lorem ipsum dolor 3",'','',$GLOBALS['db_post_slug'])) {
	        $parent_term = term_exists( 'Module 1', $GLOBALS['db_taxonomies_slug'] ); // array is returned if taxonomy is given
	        $parent_term_id = $parent_term['term_id'];
	        $args = array(   // this designates the custom post ID you want to use! make sure it's unique
	            'post_title'    => 'Lorem ipsum dolor 3',
	            'post_excerpt' => '',
	            'post_status'  => 'publish',
	            'post_type' => $GLOBALS['db_post_slug'],
	            'post_parent' => '',
	            'post_content'      => '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',
	            'tax_input'    => array(
	                $GLOBALS['db_taxonomies_slug'] => $parent_term_id
	            ),
	        );
	        wp_insert_post($args,true);
	    }
	    if(!post_exists( "Lorem ipsum dolor 4",'','',$GLOBALS['db_post_slug'])) {
	        $parent_term = term_exists( 'Module 2', $GLOBALS['db_taxonomies_slug'] ); // array is returned if taxonomy is given
	        $parent_term_id = $parent_term['term_id'];
	        $args = array(   // this designates the custom post ID you want to use! make sure it's unique
	            'post_title'    => 'Lorem ipsum dolor 4',
	            'post_excerpt' => '',
	            'post_status'  => 'publish',
	            'post_type' => $GLOBALS['db_post_slug'],
	            'post_parent' => '',
	            'post_content'      => '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',
	            'tax_input'    => array(
	                $GLOBALS['db_taxonomies_slug'] => $parent_term_id
	            ),
	        );
	        wp_insert_post($args,true);
	        add_term_meta($parent_term_id,'taxonomy_order',2);
	    }
	    if(!post_exists( "Lorem ipsum dolor 5",'','',$GLOBALS['db_post_slug'])) {
	        $parent_term = term_exists( 'Module 2', $GLOBALS['db_taxonomies_slug'] ); // array is returned if taxonomy is given
	        $parent_term_id = $parent_term['term_id'];
	        $args = array(   // this designates the custom post ID you want to use! make sure it's unique
	            'post_title'    => 'Lorem ipsum dolor 5',
	            'post_excerpt' => '',
	            'post_status'  => 'publish',
	            'post_type' => $GLOBALS['db_post_slug'],
	            'post_parent' => '',
	            'post_content'      => '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',
	            'tax_input'    => array(
	                $GLOBALS['db_taxonomies_slug'] => $parent_term_id
	            ),
	        );
	        wp_insert_post($args,true);
	    }
	    if(!post_exists( "Lorem ipsum dolor 6",'','',$GLOBALS['db_post_slug'])) {
	        $parent_term = term_exists( 'Module 2', $GLOBALS['db_taxonomies_slug'] ); // array is returned if taxonomy is given
	        $parent_term_id = $parent_term['term_id'];
	        $args = array(   // this designates the custom post ID you want to use! make sure it's unique
	            'post_title'    => 'Lorem ipsum dolor 6',
	            'post_excerpt' => '',
	            'post_status'  => 'publish',
	            'post_type' => $GLOBALS['db_post_slug'],
	            'post_parent' => '',
	            'post_content'      => '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',
	            'tax_input'    => array(
	                $GLOBALS['db_taxonomies_slug'] => $parent_term_id
	            ),
	        );
	        wp_insert_post($args,true);
	    }
	}
	add_action('admin_init','_CreateCategory');
}

?>