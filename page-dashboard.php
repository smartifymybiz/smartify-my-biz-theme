<?php 
    // Template Name: Dashboard
    get_header(); 

?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<?php
	global $wpdb;
	$table_name = $wpdb->prefix . 'custom_post';
	$count = 1;
	$current_user_id = get_current_user_id();
	$dashboard_querys = $wpdb->get_results( "SELECT * FROM $table_name ORDER BY post_order_no ASC" );
	//var_dump($dashboard_querys);
	$trecker_querys = $wpdb->get_results( "SELECT post_id FROM user_progress WHERE task_check = 1 AND post_type = '".$post_type."' AND user_id = '".$current_user_id."'" );

$primary_color = '#808080';
if(get_field( 'primary_color', 'options' )){
	$primary_color = get_field( 'primary_color', 'options' );
}
$secondary_color = '#808080';
if(get_field( 'secondary_color', 'options' )){
	$secondary_color = get_field( 'secondary_color', 'options' );
}
?>
<style type="text/css">
	.sitebtn {
	min-width: 150px;
    display: inline-block;
    text-align: center;
    padding: 10px 30px;
    background-color: <?php echo $primary_color ?>;
    color: #fff;
    font-weight: 600;
    cursor: pointer;
    border: 2px solid <?php echo $primary_color ?>;
    text-decoration: none;
}
.sitebtn:hover {
	color: <?php echo $primary_color ?>;
	background-color: #fff !important;
	text-decoration: none;
}
.sitebtn.sitebtn-gold {
	background-color: <?php echo $secondary_color ?>;
    color: #fff;
	border: 2px solid <?php echo $secondary_color ?>;
}
.sitebtn.sitebtn-gold:hover {
	background-color: #fff;
	color: <?php echo $secondary_color ?>;
}
	a{
		color: <?php echo $primary_color?>;
	}
</style>
<?php if(!empty($dashboard_querys)) : ?>
<section class="Home--dashboard">
	<div class="container">
		<div class="row">
			<?php foreach($dashboard_querys as $dashboard_query){
				$post_slug = $dashboard_query->post_slug; 
				$post_label = $dashboard_query->post_label; 
				$post_singular_label = $dashboard_query->post_singular_label; 
				$taxonomies_slug = $dashboard_query->taxonomies_slug;
				$post_image = $dashboard_query->post_image;
				$found_posts = $post_link = $post_id = array();
				$current_texonomy = get_terms( $taxonomies_slug, array( 'parent' => 0 ) );
				if(!empty( $post_slug ) && !empty($post_label) && !empty($post_singular_label) && !empty($taxonomies_slug)) {
					foreach ($current_texonomy as $texo ) {
						$texo_args = array('post_type' => $post_slug,'posts_per_page' => -1,'post_status' => 'publish');
						$texo_args['tax_query'][] = array( 'taxonomy' => $taxonomies_slug, 'field' => 'slug', 'terms' => $texo->slug );
						$texo_args_the_query = new WP_Query( $texo_args );
							if ( $texo_args_the_query->have_posts() ) : while ( $texo_args_the_query->have_posts() ) : $texo_args_the_query->the_post();
								$post_link[] = get_the_permalink();
								$post_id[] = get_the_ID();
							endwhile; endif;
						$found_posts[] = $texo_args_the_query->found_posts;
						//var_dump($post_id);
					}
					$total_post = array_sum($found_posts);
					$trecker_querys = $wpdb->get_results( "SELECT post_id FROM user_progress WHERE task_check = 1 AND post_type = '".$post_slug."' AND user_id = '".$current_user_id."'" );
					$trecker_progress = 0;
					if ( !empty($trecker_querys) ) { $trecker_id = 0;
							foreach ($trecker_querys as $trecker_query) {
								if (in_array($trecker_query->post_id, $post_id)) {
									$trecker_id++;
								}
							}
							if ($trecker_id > 0) {
								$trecker_result_count = $trecker_id;
								$trecker_progress = intval(round(( $trecker_result_count * 100 ) / $total_post));
								if ($trecker_progress > 100) { $trecker_progress = 100; } else if ($trecker_progress < 0) { $trecker_progress = 0; }
							}
						} ?>
						<div class="col-12 col-md-6 col-lg-4 mb-30">
							<div class="post-card">
								<div class="embed-responsive embed-responsive-16by9" style="background-image: url(<?php echo $post_image; ?>);"></div>
								<div class="post-card-body">
									<div class="post-tracker">
										<div class="user-progress">
											<div class="progress">
			                                	<div class="progress-bar" role="progressbar" style="width: <?php echo $trecker_progress; ?>%;" aria-valuenow="<?php echo $trecker_progress; ?>" aria-valuemin="0" aria-valuemax="100"></div>
			                            	</div>
			                            	<div class="progress-count"><span class="count-number"><?php echo $trecker_progress; ?>%</span></div>
										</div>
									</div>
									<h3 class="post-card-title"><?php echo $post_singular_label; ?></h3>
									<a href="<?php echo home_url() ?>/course-overview/?id=<?= $post_slug; ?>" class="sitebtn mt-3">Access Now</a>
								</div>
							</div>
						</div>
				<?php } $count++; } ?>
		</div>
	</div>
</section>
<?php endif;  ?>

<?php endwhile; ?>

<?php
$GLOBALS['footer_sidebar'] = false;
 get_footer(); ?>
