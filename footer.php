		</main>
		
		<!--Footer-->
		
		<footer class="Footersite <?php echo (isset($GLOBALS['footer_sidebar']) && $GLOBALS['footer_sidebar'] == true)?'indexfooter':''; ?>">
			
			<?php 
				if (get_field( 'copyright_text', 'options' )) {
					$text = get_field( 'copyright_text', 'options' );
				} else {
					$text = date('Y').' '. get_bloginfo('title') .' All rights reserved';
				}
			?>
			
			<?php if(isset($GLOBALS['footer_sidebar']) && $GLOBALS['footer_sidebar'] == true){ ?>
				<div>
					<?php echo $text; ?>
				</div>
			<?php } else { ?>
				<div class="row no-gutters">

					<?php $colclass = (isset($GLOBALS['footer_sidebar']) && $GLOBALS['footer_sidebar'] != true)?'offset-lg-3 col-lg-9':'col-lg-12'; ?>
					<div class="col-12 <?php echo $colclass; ?> text-center">
						<?php echo $text; ?>
					</div>
				</div>
			<?php } ?>

			
		</footer>
		
		<!--Scripts-->
		
		<script defer src="https://use.fontawesome.com/releases/v5.7.1/js/all.js" integrity="sha384-eVEQC9zshBn0rFj4+TU78eNA19HMNigMviK/PU/FFjLXqa/GKPgX58rvt5Z8PLs7" crossorigin="anonymous"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
		<script src="<?php echo get_template_directory_uri() . '/js/bootstrap.min.js' ?>"></script>
		<script src="<?php echo get_template_directory_uri() . '/js/owl.carousel.min.js' ?>"></script>

		<!--Warn Old Browsers To Quit Being Old-->
		
		<script>var $buoop = {c:2};function $buo_f(){var e = document.createElement("script");e.src = "//browser-update.org/update.min.js";document.body.appendChild(e);};try {document.addEventListener("DOMContentLoaded", $buo_f,false)}catch(e){window.attachEvent("onload", $buo_f)}</script> 
		
		<?php wp_footer(); ?>
	
	</body>
</html>
