/************Document Ready Functions************/

jQuery(document).ready(function () {
	
	/************MOBILE MENU************/
				
	var menu = $('.toggle-wrap');
	var header = $('.Mainmenu--cont');
		
	function mobileFiltering() {
		menu.click( function(e) {
			e.preventDefault();
			e.stopPropagation();
			e.stopImmediatePropagation();
	        var status = header.hasClass('active');
	      if(status){
	        header.removeClass('active');
	        header.slideUp(700);
	        menu.removeClass('active');
	      }else{
	        header.addClass('active');
	        header.slideDown(700);
	        menu.addClass('active');
	      }
	  });
	}
	
	function mobileDrops() {
		$('.open-children').click( function(e) {
			e.preventDefault();
			e.stopPropagation();
			e.stopImmediatePropagation();
			var link = $(this).parent();
	        var status = link.hasClass('active');
	      if(status){
	        link.removeClass('active');
	      }else{
	        link.addClass('active');
	      }
	  });
	}
	
	function endMobile() {
		menu.unbind();
		header.removeClass('active');
		$('.Mainmenu li.menu-item-has-children a').unbind();
		$('.Mainmenu li.menu-item-has-children').unbind().removeClass('active');
		$('.Mainmenu .open-children').remove();
	}
	
	function add_drop(){
		// Add drop links
		$('.Mainmenu li.menu-item-has-children').each(function() {
			$(this).append('<a class="open-children" href="#"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>');
		});
	}
	// Active mobile menu
	
	if ($(window).outerWidth() < 768) {
		add_drop();
	    mobileFiltering();
	    mobileDrops();
	} else {
		endMobile();
	}
	
	$( window ).resize(function() {
	    if($(window).outerWidth() < 768 ) {
			var loop = setInterval(add_drop(), 30);
			// some time later clear the interval
			clearInterval(loop);
	        mobileFiltering();
	        mobileDrops();
	    } else {
		    endMobile();
	    }
	});

	 $(".module-listing").click(function(){
      	var ready_career_cls = jQuery(this).attr('class').split(' ')[0];
      //console.log(ready_career_cls);
      	$(this).toggleClass("cat-toggle");
    	$("."+ready_career_cls+".cat-post").slideToggle();
    });
	
	$('#task-btn').click(function(e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		var videoIframe = $('.module-video .embed-responsive-item'); 
		var video = $(videoIframe[0]);
		console.log(video);
		video.bind('percentwatchedchanged', function (percent, lastPercent) {
			console.log(percent);
			console.log(lastPercent);
			if (percent >= .1 && lastPercent < .1) {
				console.log('The viewer has completed 25% of the lesson! 📈')
			}
		});
		var postid = $(this).attr('data-postid');
		var posttype = $(this).attr('data-posttype');
		var userid = $(this).attr('data-userid');
		var progress_value = $(this).attr('data-progress_value');
		var btn_text = "Mark As Complete";
		if(postid && posttype && userid && progress_value){
			if (parseInt(progress_value) == 0) {
				progress_value = 1
				btn_text = "Lesson Completed";
				$('#task-btn').addClass('sitebtn-gold');
			} else {
				progress_value = 0
				$('#task-btn').removeClass('sitebtn-gold');
			}
			$('#task-btn').attr('data-progress_value', progress_value);
			$('#task-btn').text(btn_text);
			 jQuery.ajax({
			 	type: "POST",
			 	url: templateUrl+"/operation.php",
			 	data: { postid : postid, posttype : posttype, userid : userid, progress_value: progress_value},
			 	success: function(status) { 
			 		console.log(status); 
			 		$('#task-btn').attr('data-progress_value', progress_value);
			 		$('#task-btn').text(btn_text);
			 	},
			 	error: function(data) { 
			 		console.log(data); 
			 	}
			 });
		}
	});
	
	

});