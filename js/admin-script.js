jQuery(document).ready(function () {
	function valid_inspiration_form () {
		var status_lable = $('#create_post_label').hasClass('valid');
		var status_singular_label = $('#create_post_singular_label').hasClass('valid');
		if(status_singular_label && status_lable) {

			$('#create-post-type').prop("disabled", false);
		} else {
			
			$('#create-post-type').prop("disabled", true);
		}
	}
	$('#create-post-type').on('click',function(e){
		 //e.preventDefault();
		if(confirm("Is this the correct course url slug? This will not be able to be changed!")){
			$('#form_create_posttype').submit();
		}

	})
	$('#create_post_label').on('input', function() {
		var input=$(this);
		var is_name=input.val();
		if(is_name){input.removeClass("invalid").addClass("valid");}
		else{input.removeClass("valid").addClass("invalid");}
		valid_inspiration_form();
	});
	$('#create_post_singular_label').on('input', function() {
		var input=$(this);
		var is_name=input.val();
		if(is_name){
			input.removeClass("invalid").addClass("valid");
		}else{
			input.removeClass("valid").addClass("invalid");
		}
		valid_inspiration_form();
	});
	$('#create_post_slug').on('input', function() {
		var input=$(this);
		var is_name=input.val();
		var value = is_name.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-').toLowerCase();
		$(input).val(value);
	});

	$('#install_dummy_content input[type=radio]').change(function() {
		var value = $(this).val();
	    if (value) {
	       	$('#btn_dummy_content').prop("disabled", false);
	    } else {
	    	$('#btn_dummy_content').prop("disabled", true);
	    }
	});

	$('#delete_post_type input[type=checkbox]').change(function() {
		var value = $("#delete_post_type input[type=checkbox]:checked").val();
	    if (value) {
	       	$('#delete_post_btn').prop("disabled", false);
	    } else {
	    	$('#delete_post_btn').prop("disabled", true);
	    }
	});

	$('.btn-switchid').on('click',function(e) {
		e.preventDefault();
jQuery('.collapse').collapse('hide');
		$('input[name="post_id[]"]').val('');
		var id = $(this).data('post_id')
		var collapse = $(this).data('collapsein');
		//alert(collapse);
		$('#'+collapse).find('input[name="post_id[]"]').val(id);
	});

	$('.post-image-uploader').on('click',function(e) {
		e.preventDefault();
		var button = $(this);
		var slug = $(button).attr('data-slug');
		custom_uploader = wp.media({
			title: 'Insert image',
			library : {
				type : 'image'
			},
			button: {
				text: 'Use this image' // button label text
			},
			multiple: false
		}).on('select', function() {
			var attachment = custom_uploader.state().get('selection').first().toJSON();
			var image = attachment.url;
			jQuery.ajax({
				type : "post",
				url : ajaxurl,
				data : {action: "post_image_upload_save", image: image, slug: slug},
				success: function(response) { 
					console.log(response);
				},
				error: function(response) { 
					console.log(response);
				}
			});
			$('#'+slug+'_picture-preview').addClass('active').find('.profile-picture').css('background-image', 'url('+attachment.url+')');
			$('#'+slug+'_remove_button').removeClass('d-none');
			$(button).addClass('d-none');
			$('#'+slug+'_image_upload_value').val(attachment.url);
		}).open();
	});

	$('.post-image-uploader_sidebar').on('click',function(e) {
		e.preventDefault();
		var button = $(this);
		var slug = $(button).attr('data-slug');
		custom_uploader = wp.media({
			title: 'Insert image',
			library : {
				type : 'image'
			},
			button: {
				text: 'Use this image' // button label text
			},
			multiple: false
		}).on('select', function() {
			var attachment = custom_uploader.state().get('selection').first().toJSON();
			var image = attachment.url;
			jQuery.ajax({
				type : "post",
				url : ajaxurl,
				data : {action: "post_sidebar_image_upload_save", image: image, slug: slug},
				success: function(response) { 
					console.log(response);
				},
				error: function(response) { 
					console.log(response);
				}
			});
			$('#'+slug+'_picture-preview_sidebar').addClass('active').find('.profile-picture').css('background-image', 'url('+attachment.url+')');
			$('#'+slug+'_remove_button_sidebar').removeClass('d-none');
			$(button).addClass('d-none');
			$('#'+slug+'_image_upload_value_sidebar').val(attachment.url);
		}).open();
	});

	$('.post_image_remove').on('click',function(e) {
		e.preventDefault();
		var button = $(this);
		var slug = $(button).attr('data-slug');
		value = '';
		jQuery.ajax({
			type : "post",
			url : ajaxurl,
			data : {action: "post_image_upload_remove", image: value, slug: slug},
			success: function(response) { 
				console.log(response);
			},
			error: function(response) { 
				console.log(response);
			}
		});
		$('#'+slug+'_picture-preview').removeClass('active').find('.profile-picture').css('background-image', 'url()');
		$(button).addClass('d-none');
		$('#'+slug+'_upload_button').removeClass('d-none');
		$('#'+slug+'_image_upload_value').val(value);
	});

	$('.post_image_remove_sidebar').on('click',function(e) {
		e.preventDefault();
		var button = $(this);
		var slug = $(button).attr('data-slug');
		value = '';
		jQuery.ajax({
			type : "post",
			url : ajaxurl,
			data : {action: "post_sidebar_image_upload_remove", image: value, slug: slug},
			success: function(response) { 
				console.log(response);
			},
			error: function(response) { 
				console.log(response);
			}
		});
		$('#'+slug+'_picture-preview_sidebar').removeClass('active').find('.profile-picture').css('background-image', 'url()');
		$(button).addClass('d-none');
		$('#'+slug+'_upload_button_sidebar').removeClass('d-none');
		$('#'+slug+'_image_upload_value_sidebar').val(value);
	});
	
	$( "#sortable_custom_post" ).sortable({
      	revert: true,
		cursor: "move",
		forcePlaceholderSize: true,
		stop: function( event, ui ) {
			$("#sortable_custom_post li").each(function(){
				var slug = ($(this).attr('data-slug'));
				var index = $(this).index() +1;
				jQuery.ajax({
					type : "post",
					url : ajaxurl,
					data : {action: "custom_post_order_no", slug: slug, index: index},
					success: function(response) { 
						console.log(response);
					},
					error: function(response) { 
						console.log(response);
					}
				});
			}); 
		}
    });
	$( "#sortable_custom_post, #sortable_custom_post li" ).disableSelection();

});

