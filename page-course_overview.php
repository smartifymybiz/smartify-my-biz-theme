<?php 
    // Template Name: Course Overview
    get_header(); 

?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); 

if( isset($_GET['id']) ):
	$post_type = $_GET['id'];

else :
	exit();
endif;
?>

<?php 
global $wpdb;
$current_user_id = get_current_user_id();
$taxonomies = get_object_taxonomies( $post_type );
$pt = get_post_type_object( $post_type );
$post_name = $pt->singular_label;
$taxonomies = $taxonomies[0];
$dtable_name = $wpdb->prefix . 'custom_post';
$dashboard_querys = $wpdb->get_results( "SELECT * FROM $dtable_name WHERE post_slug = '$post_type'" );
$sidebar_img= $dashboard_querys[0]->post_sidebar;
//$pterms_args = array('taxonomy' => $taxonomies, 'parent' => 0, 'hide_empty' => false);
$pterms_args = array(
	'taxonomy' => $taxonomies,
	'parent' => 0,
	'orderby' => 'meta_value_num',
	'order' => 'ASC',
	'hide_empty' => false,
	'meta_query' => [[
	 	'key' => 'taxonomy_order',
		'type' => 'NUMERIC',
	]],
);
$parent_terms = get_terms( $pterms_args );

$trecker_querys = $wpdb->get_results( "SELECT post_id FROM user_progress WHERE task_check = 1 AND post_type = '".$post_type."' AND user_id = '".$current_user_id."'" );

$primary_color = '#808080';
if(get_field( 'primary_color', 'options' )){
	$primary_color = get_field( 'primary_color', 'options' );
}
$secondary_color = '#808080';
if(get_field( 'secondary_color', 'options' )){
	$secondary_color = get_field( 'secondary_color', 'options' );
}
?>
<article class="single-post--page course_overview">
	<div class="container-fluid px-0">
		<div class="row no-gutters">
			<div class="col-12 col-lg-3">
				<?php $sidebar_image = array();
				if(!empty($sidebar_img)) {
					$sidebar_image['url'] = $sidebar_img;
				} else {					
					if (get_field( 'sidebar_image_option', 'options' )) {
						$sidebar_image = get_field( 'sidebar_image_option', 'options' );
					} else {
						$sidebar_image['url'] = get_template_directory_uri().'/img/sidebar.jpg';
					}
				}
				 ?>
				<div class="post-sidebar" style="background-image: url(<?php echo $sidebar_image['url']; ?>);">
					<div class="position-relative post-sidebar-data">
						<h1 class="text-white">Welcome To</h1>
						<h4 class="text-white"><?=$post_name;?></h4>
						
					</div>
					
					<div class="sidebar-container d-none">
						<p class="mb-0"><?=$post_name;?></p>
					</div>
				</div>
			</div>
			<div class="col-12 col-lg-9">
				<div class="row no-gutters justify-content-center">
					<div class="col-lg-10 col-xl-8 px-lg-5 py-lg-4 py-5 module-content">
						<?php foreach ( $parent_terms as $ptk => $pterm ) { 
							$child_arg = array(
								'taxonomy' => $taxonomies,
								'parent' => $pterm->term_id,
								'orderby' => 'meta_value_num',
								'order' => 'ASC',
								'hide_empty' => true,
								'meta_query' => [[
									'key' => 'taxonomy_order',
									'type' => 'NUMERIC',
								]],
							);
							?>
							<div class="text-center">
								<span class="mb-0 text-center mx-auto parent-cat-title"><?php echo $pterm->name; ?></span>
							</div>
							<div class="parent-cat-block pt-3 pb-4">
								<?php 
									$child_terms = get_terms( $child_arg );
							if( !empty( $child_terms ) && is_array( $child_terms ) ) {
								foreach ($child_terms as $key => $cterm) { ?>
									<div class="parent-cat-block pt-3 overflow-hidden">
										<?php 
											$args = array(
											'post_type'=> $post_type,
											'posts_per_page' => -1,
											'orderby' => 'meta_value_num',
											'order' => 'ASC',
											'tax_query' => array(
												array(
													'taxonomy' => $taxonomies,
													'field' => 'slug',
													'terms' => $cterm->slug
												)
											),
											'meta_query' => [[
												'key' => 'post__order',
												'type' => 'NUMERIC',
											]],
										);
											$tax_id = $cterm->term_id;
											$cat_slug = $cterm->slug; 
											$the_query = new WP_Query( $args );
											$post_count = $the_query->post_count;
											$trecker_progress = 0;
											$page_index  = $true = array();
											if ($post_count != 0) { ?>
												<?php 
													while ( $the_query->have_posts()): $the_query->the_post();
														$page_index[] = get_the_ID();
													endwhile;
													if ( !empty($trecker_querys) ) { $trecker_id = 0; 
														foreach ($trecker_querys as $trecker_query) {
															if (in_array($trecker_query->post_id, $page_index)) {
																$true[] = $trecker_query->post_id;
																$trecker_id++;
															}
															if ($trecker_id > 0) {
																$trecker_result_count = $trecker_id;
																$trecker_progress = intval(round(( $trecker_result_count * 100 ) / $post_count));
																if ($trecker_progress > 100) { $trecker_progress = 100; } else if ($trecker_progress < 0) { $trecker_progress = 0; }
															}
														}
													}
										 ?>

										<div class="collapse-<?php echo $tax_id; ?> px-3 px-lg-5 py-3 module-listing <?php echo ($ptk == 0)?'cat-toggle':''; ?>" style="border-left: 10px solid <?php echo $primary_color; ?>;"> 
											<div class="py-2">
												<p class="cat-main-title pl-4"><?php echo $cterm->name; ?><span class="total-post"><?php echo $post_count; ?></span><span class="float-right com-task-per com-task-per-<?php echo $tax_id; ?>"></span></p>
												<div class="progress">
													<div class="progress-bar" style="width: <?= $trecker_progress; ?>%" role="progressbar" aria-valuenow="<?= $trecker_progress; ?>" aria-valuemin="0" aria-valuemax="100" >
														</div>
												</div>
											</div>
											<div class="collapse-<?php echo $tax_id; ?> cat-post" style="display: <?php echo ($ptk == 0)?'block':'none'; ?>">
												<div class="mb-0 p-2 mod-con-1 post-title">
													<?php while ( $the_query->have_posts()): $the_query->the_post();
													$post_id = get_the_ID();
													if(in_array($post_id, $true)) {
													 	$checked_com_task = "task_true"; 
													 	// if ( get_field( 'task_true_icon', 'options' ) ) {
													 	// 	$task_iamge = get_field( 'task_true_icon', 'options' );
													 	// } else {
													 	// 	$task_iamge = get_template_directory_uri()."/img/tick-yes.png";
													 	// }
													 	$task_iamge = '<i class="fa fa-check " aria-hidden="true" style="font-size:20px;color:'.$secondary_color.'"></i>';
													} else {
														// $checked_com_task = "task_false"; 
														// if ( get_field( 'task_false_icon', 'options' ) ) {
														// 	$task_iamge = get_field( 'task_false_icon', 'options' );
														// } else {
														// 	$task_iamge = get_template_directory_uri()."/img/star.png";
														// }
														$task_iamge = '<i class="fa fa-star " aria-hidden="true" style="font-size:20px;color:'.$primary_color.'"></i>';
													}
													?>
													
														<p class="mb-0">
															<?php 
																		$choise_module_icon = get_field( "module_type" );
																		if( "video" == $choise_module_icon ) {
																			$module_icon = "video_module.png";
																		} else if( "audio" == $choise_module_icon ) {
																			$module_icon = "audio_module.png";
																		} else if( "survey" == $choise_module_icon ) {
																			$module_icon = "survey_module.png";
																		} else if( "pdf" == $choise_module_icon ) {
																			$module_icon = "pdf_module.png";
																		} else {
																			$module_icon = "survey_module.png";
																		}
																	?>
															<a class="post-module-title" href="<?php the_permalink(); ?>">
																<img src="<?php echo get_template_directory_uri() . '/img/' . $module_icon ?>">
																<span class="<?php echo $checked_com_task; ?>">
																	<?php the_title(); ?>
																</span>
																<!-- <img src="<?=$task_iamge?>" class="task--image" alt="Image"> -->
																<?=$task_iamge?>
															</a> 
														</p>
													<?php endwhile; ?>
												</div>
											</div>
										</div>

									<?php }
								?>
							</div>
						<?php }
							}else{
												echo '<div class="text-center module-listing px-3 px-lg-5 py-3" style="border-left: 10px solid '.$primary_color.'"><h6 class="mb-0" >Lesson coming soon.</h6></div>';
											}
							 ?>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>

<?php endwhile; ?>

<?php 
$GLOBALS['footer_sidebar'] = false;
get_footer(); ?>