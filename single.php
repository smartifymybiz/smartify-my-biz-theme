<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<?php 

	// global  Option
	global $wpdb;
	$current_post  = get_post();
	$current_post_title = $current_post->post_title;
	$current_post_content = $current_post->post_content;
	$current_post_content = apply_filters('the_content', $current_post_content);
	$current_post_content = str_replace(']]>', ']]&gt;', $current_post_content);
	$found_posts = $page_index = array();
	$current_user_id = get_current_user_id();
	$curreny_post_type = $current_post->post_type;
	$pt = get_post_type_object( $curreny_post_type );
	$post_name = $pt->singular_label;
	$current_post_id = $current_post->ID;
	$table_name = $wpdb->prefix . 'custom_post';       
	$single_post_querys = $wpdb->get_results( "SELECT * FROM $table_name WHERE post_slug = '".$curreny_post_type."' " );
	
	$taxonomies_slug = $single_post_querys[0]->taxonomies_slug;
	$current_post_texo = get_the_terms($current_post_id,$taxonomies_slug);

	// Total Post and Next prev pagination
	$current_texonomy_args = array(
		'taxonomy' => $taxonomies_slug,
		'parent' => 0,
		'orderby' => 'meta_value_num',
		'order' => 'ASC',
		'hide_empty' => false,
		'meta_query' => [[
			'key' => 'taxonomy_order',
			'type' => 'NUMERIC',
		]],
	);
	$current_texonomy_args_parent = array(
		'taxonomy' => $taxonomies_slug,
		'term_taxonomy_id' => $current_post_texo[0]->parent,
		'term_id'=> $current_post_texo[0]->parent,
		'orderby' => 'meta_value_num',
		'order' => 'ASC',
		'hide_empty' => false,
		'meta_query' => [[
			'key' => 'taxonomy_order',
			'type' => 'NUMERIC',
		]],
	);
	
// 	$current_texonomy = get_terms( $taxonomies_slug, array( 'parent' => 0 ) );
	$current_texonomy = get_terms( $current_texonomy_args );
	$current_texonomy_parent = get_terms( $current_texonomy_args_parent );
	$name_current_taxo_parent = $current_texonomy_parent[0]->name;
	foreach ($current_texonomy as $texo ) {
		// $texo_args = array('post_type' => $curreny_post_type,'posts_per_page' => -1,'post_status' => 'publish');
		// $texo_args['tax_query'][] = array( 'taxonomy' => $taxonomies_slug, 'field' => 'slug', 'terms' => $texo->slug );
		$texo_args = array('post_type' => $curreny_post_type,'posts_per_page' => -1,'post_status' => 'publish','orderby' => 'meta_value_num', 'order' => 'ASC');
		$texo_args['tax_query'][] = array( 'taxonomy' => $taxonomies_slug, 'field' => 'slug', 'terms' => $texo->slug );
		$texo_args['meta_query'][] = array(
			'key' => 'post__order',
			'type' => 'NUMERIC',
		);
		//print_r($texo_args);
		$texo_args_the_query = new WP_Query( $texo_args );
		$found_posts[] = $texo_args_the_query->found_posts;
		if ($texo_args_the_query->have_posts() ) : while ($texo_args_the_query->have_posts() ) : $texo_args_the_query->the_post();
			$page_index[] = get_the_ID();
		endwhile; endif;
	}
	// ORIGINAL NEXT AND PREV I WILL MOVE TO BOTTOM
	// $next_search = array_search($current_post_id,$page_index,true);
	// $next_page_link = $page_index[$next_search + 1];
	// $pre_page_link = $page_index[$next_search - 1];
	$total_post = array_sum($found_posts);
	//print_r($page_index);
	//echo 'aas';
	//echo '<div style="display:none">'.print_r($page_index)."</div>";
	//echo '<div style="display:none">'.print_r($next_search)."</div>";
	// User Traker
	$trecker_querys = $wpdb->get_results( "SELECT post_id FROM user_progress WHERE task_check = 1 AND post_type = '".$curreny_post_type."' AND user_id = '".$current_user_id."'" );
	$trecker_progress = 0;
	if ( !empty($trecker_querys) ) { $trecker_id = 0;
		foreach ($trecker_querys as $trecker_query) {
			if (in_array($trecker_query->post_id, $page_index)) {
				$trecker_id++;
			}
		}
		if ($trecker_id > 0) {
			$trecker_result_count = $trecker_id;
			$trecker_progress = intval(round(( $trecker_result_count * 100 ) / $total_post));
			if ($trecker_progress > 100) { $trecker_progress = 100; } else if ($trecker_progress < 0) { $trecker_progress = 0; }
		}
	}

	// Button Option
	$btn_value = 0;
	$btn_text = "Mark As Complete";
	$btn_class = "sitebtn";
	$btn_querys = $wpdb->get_results( "SELECT * FROM user_progress WHERE user_id = '".$current_user_id."' AND post_id = '".$current_post_id."' AND post_type ='".$curreny_post_type."'" );
	if(!empty($btn_querys)) {
		if ( $btn_querys[0]->task_check == 1 ) {
			$btn_value = 1;
			$btn_text = "Lesson Completed";
			$btn_class = "sitebtn sitebtn-gold";
		}
	}
$primary_color = '#616161';
if(get_field( 'primary_color', 'options' )){
	$primary_color = get_field( 'primary_color', 'options' );
}
$secondary_color = '#fbc85f';
if(get_field( 'secondary_color', 'options' )){
	$secondary_color = get_field( 'secondary_color', 'options' );
}
?>
<style type="text/css">
	.sitebtn {
	min-width: 150px;
    display: inline-block;
    text-align: center;
    padding: 10px 30px;
    background-color: <?php echo $primary_color ?>;
    color: #fff;
    font-weight: 600;
    cursor: pointer;
    border: 2px solid <?php echo $primary_color ?>;
    text-decoration: none;
}
.sitebtn:hover {
	color: <?php echo $primary_color ?>;
	background-color: #fff !important;
	text-decoration: none;
}
.sitebtn.sitebtn-gold {
	background-color: <?php echo $secondary_color ?>;
    color: #fff;
	border: 2px solid <?php echo $secondary_color ?>;
}
.sitebtn.sitebtn-gold:hover {
	background-color: #fff;
	color: <?php echo $secondary_color ?>;
}
		a{
		color: <?php echo $primary_color?>;
	}
	.texonomy-card .texonomy--header{
		 
	}
	.texonomy-card .texonomy--body {
   
}
.texonomy-card-title {
    text-transform: uppercase;
    font-weight: bold;
     padding-top: 5px; 
    /* border-top: 2px solid gainsboro; */
    padding-bottom: 5px;
    margin-top: 0px;
    background: #c7a5a3;
    text-align: center;
    font-size: 14px;
}
.texonomy--inner{
padding: 0px;
}
.texonomy-card {
    padding: 8px 15px;
}
</style>
<article class="single-post--page">
	<div class="container-fluid px-0">
		<div class="row no-gutters">
			<div class="col-12 col-lg-3">
				<?php  $sidebar_image = array();
					if (get_field( 'sidebar_image_option', 'options' )) {
						$sidebar_image['url'] = $single_post_querys[0]->post_sidebar;
					} else {
						$sidebar_image['url'] = get_template_directory_uri().'/img/sidebar.jpg';
					}
				 ?>
				<div class="post-sidebar" style="background-image: url(<?php echo $sidebar_image['url']; ?>);">
					<div class="post-texonomy--card w-100">
						<div class="user-tracker">
							<p class="mb-0 back-title pl-0">Back to Course Overview</p>
							<a href="<?= home_url(); ?>/course-overview/?id=<?=$curreny_post_type;?>" class="back-dashboard--link"><?= $post_name;?></a>
							<div class="user-progress">
								<div class="progress">
                                	<div class="progress-bar" role="progressbar" style="width: <?php echo $trecker_progress; ?>%;" aria-valuenow="<?php echo $trecker_progress; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                            	</div>
                            	<div class="progress-count"><span class="count-number"><?php echo $trecker_progress; ?>%</span></div>
							</div>
						</div>
						<div class="texonomy--inner">
							<?php $x=1; 
							$arrterms = array();
							//print_r($current_texonomy);
							foreach ($current_texonomy as $texonomy ) { ?>
							<div class="texonomy-card-title"><?php echo $texonomy->name; ?></div>
								<?php
							 $id=rand(1000,1000000);
								$child_arg = array(
								'taxonomy' => $taxonomies_slug,
								'parent' => $texonomy->term_id,
								'orderby' => 'meta_value_num',
								'order' => 'ASC',
								'hide_empty' => true,
								'meta_query' => [[
									'key' => 'taxonomy_order',
									'type' => 'NUMERIC',
								]],
							);
								$child_terms = get_terms( $child_arg );
							
							 ?>
							<?php 
							$class = ($name_current_taxo_parent == $texonomy->name) ? 'texonomy--header' : 'texonomy--header collapsed';
								$tab_class = ($name_current_taxo_parent == $texonomy->name) ? 'texonomy--body collapse show' : 'texonomy--body collapse';
								$expanded = ($name_current_taxo_parent == $texonomy->name) ? 'true' : 'false'; 

								// $class = ($current_post_texo[0]->slug == $texonomy->slug) ? 'texonomy--header' : 'texonomy--header collapsed';
								// $tab_class = ($current_post_texo[0]->slug == $texonomy->slug) ? 'texonomy--body collapse show' : 'texonomy--body collapse';
								// $expanded = ($current_post_texo[0]->slug == $texonomy->slug) ? 'true' : 'false'; 
							?>

							<?php 
							
							$mr = 0;
							foreach ($child_terms as $key => $cterm) {
								
								$post_args = array('post_type' => $curreny_post_type,'posts_per_page' => -1,'post_status' => 'publish','orderby' => 'meta_value_num', 'order' => 'ASC');
											$post_args['tax_query'][] = array( 'taxonomy' => $taxonomies_slug, 'field' => 'slug', 'terms' => $cterm->slug );
											$post_args['meta_query'][] = array(
												'key' => 'post__order',
												'type' => 'NUMERIC',
											);
											$post_args_the_query = new WP_Query( $post_args );
											
								// code...
								
							 ?>
							 
							 <div class="texonomy-card">
							 	<div class="<?php echo $class; ?>" type="button" data-toggle="collapse" data-target="#texonomy<?php echo $id.'_'.$x; ?>" aria-expanded="<?php echo $expanded; ?>" aria-controls="texonomy<?php echo $id.'_'.$x; ?>"><?php echo $cterm->name; ?></div>
							 	<div class="<?php echo $tab_class; ?>" id="texonomy<?php echo $id.'_'.$x; ?>">
							 	<?php if ($post_args_the_query->have_posts() ) : ?>
											<ul class="list-unstyled m-0">
												<?php while ($post_args_the_query->have_posts() ) : $post_args_the_query->the_post(); 

									$arrterms[$x][] = get_the_ID();
								
													?>
													<?php $active = ($current_post_id == get_the_ID()) ? 'active' : ''; ?>
													<li class="<?php echo $active; ?>">
														<i class="fas fa-angle-right"></i> <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
													</li>

												<?php endwhile; ?>
												<?php $mr++; ?>
											</ul>
										<?php endif; ?>
							 </div>
							 
							</div>
							
									<?php } ?>

								
							<?php $x++; } 
							
							$recount = array();
							$ias = 0;
							foreach ($arrterms as $k => $v) {
								foreach ($v as $kv => $vv) {
									$recount[] = $vv;
								}
								
							}

							$next_search = array_search($current_post_id,$recount,true);
							$next_page_link = $recount[$next_search + 1];
							$pre_page_link = $recount[$next_search - 1];
							
							//print_r($recount);
							?>

						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-lg-9">
				<section class="dashboard--header">
					<h2 class="mb-0"><?php echo $current_post_title; ?></h2>
				</section>
				<section class="post--data">
					<div class="row mx-0 justify-content-center">
						<div class="col-12 col-lg-10">
							<div class="post-module">
								<?php if( have_rows('page_content', $current_post_id) ): while ( have_rows('page_content',$current_post_id) ) : the_row(); ?>
									<?php if( get_row_layout() == 'module_video' ): ?>
										<?php if( have_rows('video', $current_post_id) ): while ( have_rows('video',$current_post_id) ): the_row(); ?>
										<div class="module-video mb-5">
											<div class="embed-responsive embed-responsive-16by9">
												<iframe class="embed-responsive-item" src="<?php the_sub_field('video_url'); ?>" ></iframe>
											</div>
										</div>
										<?php endwhile; endif; ?>
									<?php elseif( get_row_layout() == 'module_text' ):  ?>
										<div class="mb-4">
											<?php the_sub_field( 'text' ); ?>
										</div>
									<?php endif; ?>
								<?php endwhile; endif; ?>

								<ul class="list-unstyled mb-0 text-center mt-5 px-5 position-relative">
									<?php if( have_rows('download_worksheet',$current_post_id) ): while ( have_rows('download_worksheet',$current_post_id) ):  the_row(); ?>
										<li class="d-inline-block mr-2 my-2"><a  target="_blank"  href="<?php the_sub_field('download_worksheet_url'); ?>" class="sitebtn"><?php the_sub_field('worksheet_button_title'); ?></a></li>
										<?php endwhile; endif; wp_reset_postdata(); ?>
									<?php if(!empty($pre_page_link)) { ?>
										<a rel="prev" class="pre_page_link" href="<?php echo get_permalink($pre_page_link); ?>">&#8592;</a>
									<?php } ?>
									<?php if(!empty($next_page_link)) { ?>
										<a rel="prev" class="next_page_link " href="<?php echo get_permalink($next_page_link); ?>">&#8594;</a>
									<?php } ?>
									<div class="<?php echo $btn_class; ?>" id="task-btn" data-type="button" data-progress_value="<?php echo $btn_value; ?>" data-userid="<?php echo $current_user_id; ?>" data-postid="<?php echo $current_post_id; ?>" data-posttype="<?php echo $curreny_post_type; ?>"><?php echo $btn_text; ?></div>
								</ul>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</article>
<!-- <script src="//fast.wistia.net/assets/external/E-v1.js" async></script> -->

<?php endwhile; ?>
<?php 
$GLOBALS['footer_sidebar'] = false;
get_footer(); ?>
