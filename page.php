<?php get_header(); ?>

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		
		<!--Page Content-->
		<article class="single-post--page">
			<div class="container-fluid px-0">
				<div class="row no-gutters">
					<div class="col-12 col-lg-3">
						<?php  $sidebar_image = array();
							if (get_field( 'sidebar_image_option', 'options' )) {
								$sidebar_image = get_field( 'sidebar_image_option', 'options' );
							} else {
								$sidebar_image['url'] = get_template_directory_uri().'/img/sidebar.jpg';
							}
						 ?>
						<div class="post-sidebar page_option_sidebar" style="background-image: url(<?php echo $sidebar_image['url']; ?>);">
							
						</div>
					</div>
					<div class="col-12 col-lg-9">
						<section class="dashboard--header">
							<h2 class="mb-0"><?php the_title(); ?></h2>
						</section>
						<section class="post--data">
							<div class="row mx-0 justify-content-center">
								<div class="col-12 col-lg-10">
									<div class="post-module">
										<?php the_content(); ?>
									</div>
								</div>
							</div>
						</section>
					</div>

				</div>
			</div>
		</article>
		
	<?php endwhile; ?>

<?php get_footer(); ?>
