<!doctype html>
<html lang="en">
	<head>
	  	<meta charset="utf-8">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">		
		<?php if ( is_front_page() || is_home() ) { ?>
			<title><?php bloginfo( 'name' ); echo ' - '; bloginfo('description');  ?></title>
		<?php }else{ ?>
			<title><?php bloginfo( 'name' ); echo ' - '; wp_title(''); ?></title>
		<?php } ?>
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
		<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
		<script>
            var admin_url = '<?= admin_url(); ?>';
            var ajax_url = admin_url + 'admin-ajax.php';
            var templateUrl = '<?= get_bloginfo("template_url"); ?>';
        </script> 
		<?php wp_head(); ?>
	</head>
	
	<body <?php body_class(); ?>>
		<!--Header-->
		<header class="site-header">
			<div class="container">
				<div class="row justify-content-between align-items-center">
					<div class="col-3 col-lg-auto logo-container">
						<?php
							$custom_logo_id = get_theme_mod( 'custom_logo' );
							$image = wp_get_attachment_image_src( $custom_logo_id , 'full' ); 
						?>
						<?php 
							$image = array();
							if (get_field( 'logo', 'options' )) {
								$image = get_field( 'logo', 'options' );
							} else {
								$image['url'] = get_template_directory_uri().'/img/logo.png';
							}
						?>
						<a class="logo d-inline-block" href="<?php bloginfo('url');?>"><img src="<?php echo $image['url'] ?>" alt="<?php bloginfo('title');?>"/></a>
					</div>
					<div class="col-9 col-lg-8 text-right">
						<button class="mobile-menu ml-auto d-none">
							<i class="fas fa-bars"></i>
						</button>
						<nav>
							<button class="mobile-menu ml-auto d-none">
								<i class="fas fa-times"></i>
							</button>
							<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => false, 'menu_class' => 'header_menu list-unstyled mb-0' )); ?></nav>
					</div>
				</div>	
			</div>
		</header>

		<!--Main Content-->
		
		<main id="main" class="<?php if(is_front_page()) { echo 'home_main';} ?>">