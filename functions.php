<?php
require get_template_directory() . '/includes/create-table.php';
require get_template_directory() . '/includes/theme-functions.php';



require_once get_template_directory() . '/includes/membership-plugin-activation.php';


function custom_permalinks() {
    if (isset($_GET['activated']) && is_admin()) {
        global $wp_rewrite;
        $wp_rewrite->set_permalink_structure( $wp_rewrite->root . '/%postname%/' ); // custom post permalinks
    }
}
add_action( 'init', 'custom_permalinks' );

if (isset($_GET['activated']) && is_admin()) {

    // Home Page Create
    $home_title = 'Home';
    $home_content = '';
    $home_page_template = 'page-dashboard.php'; //ex. template-custom.php. Leave blank if you don't want a custom page template.
    $home_check = get_page_by_title($home_title);
    $home_page = array( 'post_type' => 'page', 'post_title' => $home_title, 'post_content' => $home_content, 'post_status' => 'publish', 'post_author' => 1, );
    if(!isset($home_check->ID)) {
        $home_page_id = wp_insert_post($home_page);
        if(!empty($home_page_template)){ update_post_meta($home_page_id, '_wp_page_template', $home_page_template); }
        update_option( 'page_on_front', $home_page_id );
        update_option( 'show_on_front', 'page' );
    }
    // Course Overview Page Create
    $course_title = 'Course Overview';
    $course_content = '';
    $course_page_template = 'page-course_overview.php'; //ex. template-custom.php. Leave blank if you don't want a custom page template.
    $course_check = get_page_by_title($course_title);
    $course_page = array( 'post_type' => 'page', 'post_title' => $course_title, 'post_content' => $course_content, 'post_status' => 'publish', 'post_author' => 1, );
    if(!isset($course_check->ID)){
        $course_page_id = wp_insert_post($course_page);
        if(!empty($course_page_template)){ update_post_meta($course_page_id, '_wp_page_template', $course_page_template); }
    }

    // Login Page Create
    $login_title = 'Login';
    $login_content = '';     
    $login_content .= '<!-- wp:shortcode -->';
    $login_content .= "[login_page forgotpw='false']";
    $login_content .= '<!-- /wp:shortcode -->';

    $login_content .= '<!-- wp:paragraph -->';
    $login_content .= '<p><a href="/forgot-password/">Forgot Password?</a></p>';
    $login_content .= '<!-- /wp:paragraph -->';

    $login_page_template = 'page-login.php'; //ex. template-custom.php. Leave blank if you don't want a custom page template.
    $login_check = get_page_by_title($login_title);
    $login_page = array( 'post_type' => 'page', 'post_title' => $login_title, 'post_content' => $login_content, 'post_status' => 'publish', 'post_author' => 1, );
    if(!isset($login_check->ID)){
        $login_page_id = wp_insert_post($login_page);
        if(!empty($login_page_template)){ update_post_meta($login_page_id, '_wp_page_template', $login_page_template); }
    }

    // Forgot password Create
    $forgotpassword_title = 'Forgot Password';
    $forgotpassword_content = '';
    $forgotpassword_content .= '<!-- wp:paragraph -->';
    $forgotpassword_content .= '<p><strong>Enter your email below!</strong></p>';
    $forgotpassword_content .= '<!-- /wp:paragraph -->';

    $forgotpassword_content .= '<!-- wp:paragraph -->';
    $forgotpassword_content .= '<p>Check your inbox. We will send your login details shortly.</p>';
    $forgotpassword_content .= '<!-- /wp:paragraph -->';

    $forgotpassword_content .= '<!-- wp:paragraph -->';
    $forgotpassword_content .= '<p>If you haven&#39;t seen the message, check your SPAM folder.</p>';
    $forgotpassword_content .= '<!-- /wp:paragraph -->';

    $forgotpassword_content .= '<!-- wp:shortcode -->';
    $forgotpassword_content .= '<script type="text/javascript" async="true" src="https://app.ontraport.com/js/ontraport/opt_assets/drivers/opf.js" data-opf-uid="p2c238324f1" data-opf-params="borderColor=#000000&amp;borderSize=0px&amp;embed=true&amp;formHeight=38&amp;formWidth=100%&amp;popPosition=mc&amp;instance=1866728706"></script>';
    $forgotpassword_content .= '<!-- /wp:shortcode -->';

    $forgotpassword_template = 'page-forgotpassword.php'; //ex. template-custom.php. Leave blank if you don't want a custom page template.
    $forgotpassword_check = get_page_by_title($forgotpassword_title);
    $forgotpassword_page = array( 'post_type' => 'page', 'post_title' => $forgotpassword_title, 'post_content' => $forgotpassword_content, 'post_status' => 'publish', 'post_author' => 1, );
    if(!isset($forgotpassword_check->ID)){
        $forgotpassword_id = wp_insert_post($forgotpassword_page);
        if(!empty($forgotpassword_template)){ update_post_meta($forgotpassword_id, '_wp_page_template', $forgotpassword_template); }
    }

    // Create Menu
    $menuname = "Header Menu";
    $menu_location = 'primary';
    $menu_exists = wp_get_nav_menu_object( $menuname );
    if( !$menu_exists) {
        $menu_id = wp_create_nav_menu($menuname);
        wp_update_nav_menu_item($menu_id, 0, array(
            'menu-item-title' =>  __('Home'),
            'menu-item-classes' => 'home',
            'menu-item-url' => home_url( '/' ), 
            'menu-item-status' => 'publish')
        );
        if( !has_nav_menu( $menu_location ) ){
            $locations = get_theme_mod('nav_menu_locations');
            $locations[$menu_location] = $menu_id;
            set_theme_mod( 'nav_menu_locations', $locations );
        }
    }
}

// ******************* Load File ****************** //

function sunset_load_scripts(){

    $number = rand(1, 1000000); 
    $var2 = "$number";
    
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '4.4.1', 'all' );
    wp_enqueue_style( 'owl-main', get_template_directory_uri() . '/css/owl.carousel.min.css', array(), '2.3.4', 'all' );
    wp_enqueue_style( 'owl-theme-default', get_template_directory_uri() . '/css/owl.theme.default.min.css', array(), '2.3.4', 'all' );
    wp_enqueue_style( 'main-style', get_template_directory_uri() . '/style.css', array(), $var2 , 'all' );
    wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/js/script.js', array(), '1.2.3', true );
    
}
add_action( 'wp_enqueue_scripts', 'sunset_load_scripts' );

// ******************* Menu Register ****************** //

function sunset_register_nav_menu()
{
    register_nav_menu('primary', 'Header Navigation Menu');


    global $wpdb;
    $table_name = $wpdb->prefix . 'custom_post';
    $register_post_querys = $wpdb->get_results( "SELECT * FROM $table_name" );
    if(!empty($register_post_querys)) {
        foreach($register_post_querys as $register_post_query){
            if(!empty( $register_post_query->post_slug ) && !empty($register_post_query->post_label) && !empty($register_post_query->post_singular_label) && !empty($register_post_query->taxonomies_slug)) {
                $post_slug = $register_post_query->post_slug;
                $post_label = $register_post_query->post_label;
                $post_singular_label = $register_post_query->post_singular_label;
                $taxonomies_slug = $register_post_query->taxonomies_slug;
                if(!post_type_exists($post_slug)) {
                    register_post_type($post_slug, array(
                        'label' => __($post_label),
                        'singular_label' => __($post_singular_label),
                        'public' => true,
                        'show_ui' => true,
                        'capability_type' => 'post',
                        'hierarchical' => false,
                        'rewrite' => true,
                        'query_var' => false,
                        'has_archive' => false,
                        'supports' => array('title', 'editor', 'thumbnail')
                    ));
                    register_taxonomy( $taxonomies_slug, $post_slug, array( 'hierarchical' => true, 'label' => 'All Module', 'query_var' => true, 'rewrite' => true ) );
                }
            }
        }
        
    }
}
add_action('after_setup_theme', 'sunset_register_nav_menu');

/* Activate HTML5 features */
add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));

// ******************* Add Custom Excerpt Lengths ****************** //

function custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// ******************* Custom Logo ****************** //
add_theme_support( 'custom-logo' );

// ******************* Sidebars ****************** //

if ( function_exists('register_sidebar') ) {
    register_sidebar(array(
        'name' => 'Blog',
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));
}

// ******************* Add Custom Menus ****************** //

add_theme_support( 'menus' );

// ******************* Add Custom Excerpt Lengths ****************** //

function wpe_excerptlength_index($length) {
    return 160;
}
function wpe_excerptmore($more) {
    return '...<a href="'. get_permalink().'">Read More ></a>';
}

function wpe_excerpt($length_callback='', $more_callback='') {
    global $post;
    if(function_exists($length_callback)){
        add_filter('excerpt_length', $length_callback);
    }
    if(function_exists($more_callback)){
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>'.$output.'</p>';
    echo $output;
}

// ******************* Add Post Thumbnails ****************** //

add_theme_support( 'post-thumbnails' );
//set_post_thumbnail_size( 50, 50, true );
add_image_size( 'xlarge', 1200, 1200);


// ******************* ACF Options Page ****************** //

if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'    => 'Theme Options',
        'menu_title'    => 'Theme Options',
        'menu_slug'     => 'theme-general-options',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
    
}


// ******************* Add SVG Upload Support ****************** //

function wpcontent_svg_mime_type( $mimes = array() ) {
  $mimes['svg']  = 'image/svg+xml';
  $mimes['svgz'] = 'image/svg+xml';
  return $mimes;
}
add_filter( 'upload_mimes', 'wpcontent_svg_mime_type' );

add_filter( 'wp_get_attachment_image_src', 'fix_wp_get_attachment_image_svg', 10, 4 );  /* the hook */

 function fix_wp_get_attachment_image_svg($image, $attachment_id, $size, $icon) {
    if (is_array($image) && preg_match('/\.svg$/i', $image[0]) && $image[1] <= 1) {
        if(is_array($size)) {
            $image[1] = $size[0];
            $image[2] = $size[1];
        } elseif(($xml = simplexml_load_file($image[0])) !== false) {
            $attr = $xml->attributes();
            $viewbox = explode(' ', $attr->viewBox);
            $image[1] = isset($attr->width) && preg_match('/\d+/', $attr->width, $value) ? (int) $value[0] : (count($viewbox) == 4 ? (int) $viewbox[2] : null);
            $image[2] = isset($attr->height) && preg_match('/\d+/', $attr->height, $value) ? (int) $value[0] : (count($viewbox) == 4 ? (int) $viewbox[3] : null);
        } else {
            $image[1] = $image[2] = null;
        }
    }
    return $image;
} 

// ******************* Remove Admin Bar ****************** //
add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin() ) {
        show_admin_bar(false);
    }
}

add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
    .theme-browser .theme.active .theme-screenshot img {
        top: 50%; transform: translateY(-50%);
    } 
  </style>';
}

add_action('wp_ajax_post_image_upload_save', 'post_image_upload_save_callback');
add_action('wp_ajax_nopriv_post_image_upload_save', 'post_image_upload_save_callback');
function post_image_upload_save_callback() {
    $image = '';
    $image = $_POST['image'];
    $post_type = $_POST['slug'];
    global $wpdb;
    $table_name = $wpdb->prefix .'custom_post';
    $wpdb->query( "UPDATE $table_name SET post_image ='$image' WHERE post_slug = '".$post_type."' ");
    wp_die();
}

add_action('wp_ajax_post_sidebar_image_upload_save', 'post_sidebar_image_upload_save_callback');
add_action('wp_ajax_nopriv_post_sidebar_image_upload_save', 'post_sidebar_image_upload_save_callback');
function post_sidebar_image_upload_save_callback() {
    $image = '';
    $image = $_POST['image'];
    $post_type = $_POST['slug'];
    global $wpdb;
    $table_name = $wpdb->prefix .'custom_post';
    $wpdb->query( "UPDATE $table_name SET post_sidebar ='$image' WHERE post_slug = '".$post_type."' ");
    wp_die();
}

add_action('wp_ajax_post_image_upload_remove', 'post_image_upload_remove_callback');
add_action('wp_ajax_nopriv_post_image_upload_remove', 'post_image_upload_remove_callback');
function post_image_upload_remove_callback() {
    $image = '';
    $image = $_POST['image'];
    $post_type = $_POST['slug'];
    global $wpdb;
    $table_name = $wpdb->prefix .'custom_post';
    $wpdb->query( "UPDATE $table_name SET post_image ='$image' WHERE post_slug = '".$post_type."' ");
    wp_die();
}

add_action('wp_ajax_post_sidebar_image_upload_remove', 'post_sidebar_image_upload_remove_callback');
add_action('wp_ajax_nopriv_post_sidebar_image_upload_remove', 'post_sidebar_image_upload_remove_callback');
function post_sidebar_image_upload_remove_callback() {
    $image = '';
    $image = $_POST['image'];
    $post_type = $_POST['slug'];
    global $wpdb;
    $table_name = $wpdb->prefix .'custom_post';
    $wpdb->query( "UPDATE $table_name SET post_sidebar ='$image' WHERE post_slug = '".$post_type."' ");
    wp_die();
}

add_action('wp_ajax_custom_post_order_no', 'custom_post_order_no_callback');
add_action('wp_ajax_nopriv_custom_post_order_no', 'custom_post_order_no_callback');
function custom_post_order_no_callback() {
    $index = intval($_POST['index']);
    $post_type = $_POST['slug'];
    global $wpdb;
    $table_name = $wpdb->prefix .'custom_post';
    $wpdb->query( "UPDATE $table_name SET post_order_no ='$index' WHERE post_slug = '".$post_type."' ");
    wp_die();
}

// remove wp version number from scripts and styles
function remove_css_js_version( $src ) {
    if( strpos( $src, '?ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'remove_css_js_version', 9999 );
add_filter( 'script_loader_src', 'remove_css_js_version', 9999 );
?>
