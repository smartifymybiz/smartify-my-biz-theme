<!-- this code is for ontraport redirect base on access level
so create a page like a page name is  "process", after login is should redirect to process page, and the process page will redirect which page should be redirected
 -->

[show_if has_one="Inconsistent Entrepreneur"]

<META http-equiv="refresh" content="0;URL='/course-inconsistent-entrepreneur/'" /> 

[/show_if]

[show_if has_one="Member"]

<META http-equiv="refresh" content="0;URL='https://ihaveadhd.com/member-login/'" /> 

[/show_if]

[show_if has_one="Coach Certification"]

<META http-equiv="refresh" content="0;URL='https://ihaveadhd.com/course-coach-certification/'" /> 

[/show_if]
