<?php // Template Name: Login Page
get_header('login');

$primary_color = '#000';
if(get_field( 'primary_color', 'options' )){
	$primary_color = get_field( 'primary_color', 'options' );
}
$secondary_color = '#000';
if(get_field( 'secondary_color', 'options' )){
	$secondary_color = get_field( 'secondary_color', 'options' );
}
?>
<style type="text/css">
	.post-module .op-login-form-1 INPUT[type=submit]:hover {
		color: <?php echo $primary_color ?>;
	}
	.post-module .op-login-form-1 INPUT[type=submit] {
		border: 2px solid <?php echo $primary_color ?>;
		background-color: <?php echo $primary_color ?>;
	}
</style>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<!--Page Content-->
<article class="single-post--page">
	<div class="container-fluid px-0">
		<div class="row no-gutters">
			<div class="col-12 col-lg-3">
				<?php  $sidebar_image = array();
				if (get_field( 'sidebar_image_option', 'options' )) {
					$sidebar_image = get_field( 'sidebar_image_option', 'options' );
				} else {
					$sidebar_image['url'] = get_template_directory_uri().'/img/sidebar.jpg';
				}
				?>
				<div class="post-sidebar login-sidebar page_option_sidebar" style="background-image: url(<?php echo $sidebar_image['url']; ?>);">
					<div>
						<div class="position-relative post-sidebar-data">
							<h1 class="text-white mb-3">Welcome To</h1>
							<h5 class="text-white"><?php echo get_bloginfo('title') ?></h5>
						</div>

						<div class="position-relative mt-4 pt-0">
							<?php
							$custom_logo_id = get_theme_mod( 'custom_logo' );
							$image = wp_get_attachment_image_src( $custom_logo_id , 'full' ); 
							?>
							<?php 
							$image = array();
							if (get_field( 'logo', 'options' )) {
								$image = get_field( 'logo', 'options' );
							} else {
								$image['url'] = get_template_directory_uri().'/img/logo.png';
							}
							?>
							<!-- <a class="logo d-inline-block" href="<?php bloginfo('url');?>">
								<img src="<?php echo $image['url'] ?>" alt="<?php bloginfo('title');?>"/>
							</a> -->
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-lg-9">
				<section class="dashboard--header d-none">
					<h2 class="mb-0 text-center "><?php the_title(); ?></h2>
				</section>
				<section class="post--data h-100">
					<div class="row mx-0 justify-content-center h-100 align-items-center">
						<div class="col-12 col-lg-10">
							<div class="post-module no-background">
								<h2 class="text-center mb-4"><?php the_title(); ?></h2>
								<div class="op-login-container">
									<?php echo do_shortcode(apply_filters('the_content', $post->post_content)); ?>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>

		</div>
	</div>
</article>

<?php endwhile; ?>

<?php 
$GLOBALS['footer_sidebar'] = true;
get_footer();
 ?>
