<!doctype html>
<html lang="en">
	<head>
	  	<meta charset="utf-8">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">		
		<?php if ( is_front_page() || is_home() ) { ?>
			<title><?php bloginfo( 'name' ); echo ' - '; bloginfo('description');  ?></title>
		<?php }else{ ?>
			<title><?php bloginfo( 'name' ); echo ' - '; wp_title(''); ?></title>
		<?php } ?>
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
		<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
		<script>
            var admin_url = '<?= admin_url(); ?>';
            var ajax_url = admin_url + 'admin-ajax.php';
            var templateUrl = '<?= get_bloginfo("template_url"); ?>';
        </script> 
		<?php wp_head(); ?>
	</head>
	
	<body <?php body_class(); ?>>
		<!--Header-->
	

		<!--Main Content-->
		
		<main id="main" class="<?php if(is_front_page()) { echo 'home_main';} ?>">